require 'qr_code_helper/view_helpers'
module QrCodeHelper
  class Railtie < Rails::Railtie
    initializer "qr_code_helper.view_helpers" do
      ActionView::Base.send :include, ViewHelpers
    end
  end
end
