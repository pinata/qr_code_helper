# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'qr_code_helper/version'

Gem::Specification.new do |spec|
  spec.name          = "qr_code_helper"
  spec.version       = QrCodeHelper::VERSION
  spec.authors       = ["Florent FERRY"]
  spec.email         = ["florentferry@users.noreply.github.com"]

  spec.summary       = %q{Helper to render QRCode}
  spec.description   = %q{Helper to render QRCode}
  spec.homepage      = "https://github.com/florentferry/qr_code_helper"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.9"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_runtime_dependency "rqrcode"
end
